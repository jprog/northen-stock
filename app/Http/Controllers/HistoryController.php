<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProdukInven;

class HistoryController extends Controller
{
    public function index(Request $request)
    {
        $tanggal = null;
        $inven = ProdukInven::with(['produk', 'user'])->whereNotNull('produk_id');
        if($request->tanggal != "" && $request->tanggal != null) {
            $tanggal = $request->tanggal;
            $inven->whereDate('created_at', $tanggal);
        }
        $inven = $inven->get();
        return view('history', ['inven' => $inven, 'tanggal' => $tanggal]);
    }
}
