<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <table>
        <thead>
            <tr>
                <td colspan="8" style="text-align: center; font-size: 20px;"><b>LAPORAN PRODUK</b></td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><b>Kode</b></td>
                <td><b>Nama Produk</b></td>
                <td><b>Kategori</b></td>
                <td><b>Stok Awal</b></td>
                <td><b>Barang Masuk</b></td>
                <td><b>Barang Keluar</b></td>
                <td><b>Stok Akhir</b></td>
                <td><b>Masa Kadaluarsa</b></td>
            </tr>
            @foreach($produk as $p)
            <tr>
                <td>{{$p->id}}</td>
                <td>{{$p->nama}}</td>
                <td>{{$p->kategori}}</td>
                <td>{{$p->qty_awal}}</td>
                @php
                $isMasuk = false;
                $isKeluar = false;
                @endphp
                @foreach($p->produk_inven_masuk as $pi)
                    @php
                    $isMasuk = true;
                    @endphp
                    <td>{{$pi->jumlah}} pada {{$pi->created_at}}</td>
                    @break
                @endforeach
                @if(!$isMasuk)
                <td></td>
                @endif
                @foreach($p->produk_inven_keluar as $pi)
                    @php
                    $isKeluar = true;
                    @endphp
                    <td>{{$pi->jumlah}} pada {{$pi->created_at}}</td>
                    @break
                @endforeach
                @if(!$isKeluar)
                <td></td>
                @endif
                <td>{{$p->qty_akhir}}</td>
                @foreach($p->kadaluarsa as $k)
                    <td>{{$k->jumlah}} kadaluarsa pada {{substr($k->kadaluarsa, 0, 10)}} (sisa {{$k->sisa_hari}} hari)</td>
                    @break
                @endforeach
            </tr>
            @php
            $max = $p->produk_inven_keluar->count();
            if($max < $p->produk_inven_masuk->count())
                $max = $p->produk_inven_masuk->count();
            if($max < $p->kadaluarsa->count())
                $max = $p->kadaluarsa->count();
            @endphp
            @for($i=1; $i<$max; $i++)
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    @if($i < $p->produk_inven_masuk->count())
                    <td>{{$p->produk_inven_masuk[$i]->jumlah}} pada {{$p->produk_inven_masuk[$i]->created_at}}</td>
                    @else
                    <td></td>
                    @endif
                    @if($i < $p->produk_inven_keluar->count())
                    <td>{{$p->produk_inven_keluar[$i]->jumlah}} pada {{$p->produk_inven_keluar[$i]->created_at}}</td>
                    @else
                    <td></td>
                    @endif
                    <td></td>
                    @if($i < $p->kadaluarsa->count())
                    <td>{{$p->kadaluarsa[$i]->jumlah}} kadaluarsa pada {{substr($p->kadaluarsa[$i]->kadaluarsa, 0, 10)}} (sisa {{$p->kadaluarsa[$i]->sisa_hari}} hari)</td>
                    @else
                    <td></td>
                    @endif
                </tr>
            @endfor
            @endforeach
        </tbody>
    </table>
</html>