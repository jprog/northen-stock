<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <table>
        <thead>
            <tr>
                <td colspan="9" style="text-align: center; font-size: 20px;"><b>LAPORAN HASIL STOK OPNAME</b></td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><b>Kode</b></td>
                <td><b>Nama Produk</b></td>
                <td><b>Kategori</b></td>
                <td><b>Stok Awal</b></td>
                <td><b>Barang Masuk</b></td>
                <td><b>Barang Keluar</b></td>
                <td><b>Stok Akhir</b></td>
                <td><b>Stok Aktual</b></td>
                <td><b>Selisih</b></td>
            </tr>

            @foreach($produk as $p)
            @php
            $max = 0;
            $isOd = true;
            $od = $p->opname_detail->where('opname_id', $opname->id)->last();
            @endphp
            <tr>
                <td>{{$p->id}}</td>
                <td>{{$p->nama}}</td>
                <td>{{$p->kategori}}</td>
                @if($od != null)
                <td>{{$od->qty_awal}}</td>
                @else
                <td>{{$p->qty_awal}}</td>
                @endif
                <td>
                @if($od != null && $od->opname_detail_produk != null)
                @php
                $p->produk_inven_masuk = $od->opname_detail_produk->where('jenis', 'Masuk')->values();
                if($max < $p->produk_inven_masuk->count())
                    $max = $p->produk_inven_masuk->count();
                @endphp
                @for($i=0;$i<$od->opname_detail_produk->count();$i++)
                @if($od->opname_detail_produk[$i]->jenis == 'Masuk')
                {{$od->opname_detail_produk[$i]->content}}
                @break
                @endif
                @endfor
                @else
                @php
                if($max < $p->produk_inven_masuk->count())
                    $max = $p->produk_inven_masuk->count();
                $isOd = false;
                @endphp
                @for($i=0;$i<$p->produk_inven_masuk->count();$i++)
                {{$p->produk_inven_masuk[$i]->jumlah}} pada {{$p->produk_inven_masuk[$i]->created_at}}
                @break
                @endfor
                </td>
                @endif
                <td>
                @if($od != null && $od->opname_detail_produk != null)
                @php
                $p->produk_inven_keluar = $od->opname_detail_produk->where('jenis', 'Keluar')->values();
                if($max < $p->produk_inven_keluar->count())
                    $max = $p->produk_inven_keluar->count();
                @endphp
                @for($i=0;$i<$od->opname_detail_produk->count();$i++)
                @if($od->opname_detail_produk[$i]->jenis == 'Keluar')
                {{$od->opname_detail_produk[$i]->content}}
                @break
                @endif
                @endfor
                @else
                @php
                if($max < $p->produk_inven_keluar->count())
                    $max = $p->produk_inven_keluar->count();
                @endphp
                @for($i=0;$i<$p->produk_inven_keluar->count();$i++)
                {{$p->produk_inven_keluar[$i]->jumlah}} pada {{$p->produk_inven_keluar[$i]->created_at}}
                @break
                @endfor
                @endif
                </td>
                @if($od != null)
                <td>{{$od->qty_system}}</td>
                @else
                <td>{{$p->qty_akhir}}</td>
                @endif
                @if($od != null)
                <td>{{$od->qty_actual}}</td>
                @else
                <td></td>
                @endif
                @if($od != null)
                <td>{{abs($od->qty_actual-$od->qty_system)}}</td>
                @else
                <td></td>
                @endif
            </tr>
            @for($i=1; $i<$max; $i++)
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    @if($isOd)
                    @if($i < $p->produk_inven_masuk->count())
                    <td>{{$p->produk_inven_masuk[$i]->content}}</td>
                    @else
                    <td></td>
                    @endif
                    @else
                    @if($i < $p->produk_inven_masuk->count())
                    <td>{{$p->produk_inven_masuk[$i]->jumlah}} pada {{$p->produk_inven_masuk[$i]->created_at}}</td>
                    @else
                    <td></td>
                    @endif
                    @endif

                    @if($isOd)
                    @if($i < $p->produk_inven_keluar->count())
                    <td>{{$p->produk_inven_keluar[$i]->content}}</td>
                    @else
                    <td></td>
                    @endif
                    @else
                    @if($i < $p->produk_inven_keluar->count())
                    <td>{{$p->produk_inven_keluar[$i]->jumlah}} pada {{$p->produk_inven_keluar[$i]->created_at}}</td>
                    @else
                    <td></td>
                    @endif
                    @endif

                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            @endfor
            @endforeach
        </tbody>
    </table>
</html>