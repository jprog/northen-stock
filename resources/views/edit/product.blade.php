<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Northen Stock</title>

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous"/>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.js" integrity="sha512-nO7wgHUoWPYGCNriyGzcFwPSF+bPDOR+NvtOYy2wMcWkrnCNPKBcFEkU80XIN14UVja0Gdnff9EmydyLlOL7mQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdn.jsdelivr.net/gh/xcash/bootstrap-autocomplete@v2.3.7/dist/latest/bootstrap-autocomplete.min.js"></script>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg" style="background-color: #D3D3D3;">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Northen Stock</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                <li class="nav-item">
                <a class="nav-link" href="{{ route('home') }}">Beranda</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="{{ route('product') }}">Daftar Stock</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="#">Stock Opname</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="#">Riwayat</a>
                </li>
                @if(session('user')->role == 'Admin')
                <li class="nav-item">
                <a class="nav-link" href="{{ route('employee') }}">Karyawan</a>
                </li>
                @endif
                <li class="nav-item">
                <a class="nav-link" href="#">Pesan</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="{{ route('logout') }}">Logout</a>
                </li>
            </ul>
            </div>
        </div>
        </nav>
        <div class="container-fluid">
            <div class="d-flex flex-row mt-3">
                @if($id == null)
                <h3>Tambah Produk Baru</h3>
                @else
                <h3>Edit Produk</h3>
                @endif
            </div>
            <div class="row mt-3">
                <div class="col">
                    <div class="card shadow">
                        <div class="card-body">
                            <form method="post" action="{{ route('product.post') }}">
                                @csrf
                                @if($id != null)
                                <input type="hidden" name="id" value="{{$id}}"/>
                                @endif
                                <div class="row">
                                    <div class="col">
                                        <label for="nama" class="form-label">Nama Produk</label>
                                        @if($id == null)
                                        <input type="text" id="nama" class="form-control" name="nama" maxlength="100" required>
                                        @else
                                        <input type="text" id="nama" class="form-control" name="nama" maxlength="100" value="{{$produk->nama}}" required>
                                        @endif
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col">
                                        <label for="kategori" class="form-label">Kategori</label>
                                        @if($id == null)
                                        <input type="text" id="kategori" autocomplete="off" class="form-control basicAutoComplete" name="kategori" maxlength="50" required>
                                        @else
                                        <input type="text" id="kategori" autocomplete="off" class="form-control basicAutoComplete" name="kategori" maxlength="50" value="{{$produk->kategori}}" required>
                                        @endif
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col">
                                        @if($id == null)
                                        <label for="stok" class="form-label">Stok Awal</label>
                                        <input type="number" id="stok" name="qty_awal" min="0" class="form-control" required/>
                                        @else
                                        <label for="stok" class="form-label">Stok Awal</label>
                                        @if($produk->produk_inven->count() > 0)
                                        @if($produk->opname_detail->count() > 0)
                                        <input type="number" id="stok" class="form-control" value="{{ $produk->opname_detail->last()->qty_actual }}" disabled/>
                                        @else
                                        <input type="number" id="stok" class="form-control" value="{{ $produk->qty_awal }}" disabled/>
                                        @endif
                                        <small class="text-secondary">Harap melakukan update stok di halaman awal atau stock opname</small>
                                        @else
                                        <input type="number" id="stok" name="qty_awal" min="0" class="form-control" value="{{ $produk->qty_awal }}" required/>
                                        @endif
                                        @endif
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col">
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script>
            let availableTags = [];
            let kategori = @json($kategori);
            for(var i=0;i<kategori.length; i++)
                availableTags.push(kategori[i].kategori);
            $(document).ready(function() {
                $(".basicAutoComplete").autoComplete({
                    minLength: 1,
                    events: {
                        search: function(qry, callback) {
                            let finalSearch = availableTags.filter(item => item.toLowerCase().includes(qry.toLowerCase()));
                            callback(finalSearch);
                        }
                    }
                });
            });
        </script>

        <footer class="p-2" style="width: 100%; margin-top: 200px; background-color: #D3D3D3;">
            <div class="container my-auto">
            <div class="copyright text-center text-black my-auto">
                Copyright © Northen Stock 2023
            </div>
            </div>
        </footer>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    </body>
</html>