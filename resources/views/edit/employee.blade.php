<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Northen Stock</title>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.js" integrity="sha512-nO7wgHUoWPYGCNriyGzcFwPSF+bPDOR+NvtOYy2wMcWkrnCNPKBcFEkU80XIN14UVja0Gdnff9EmydyLlOL7mQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    </head>
    <body>
        <nav class="navbar navbar-expand-lg" style="background-color: #D3D3D3;">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Northen Stock</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                <li class="nav-item">
                <a class="nav-link" href="{{ route('home') }}">Beranda</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="{{ route('product') }}">Daftar Stock</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="#">Stock Opname</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="#">Riwayat</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="{{ route('employee') }}">Karyawan</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="#">Pesan</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="{{ route('logout') }}">Logout</a>
                </li>
            </ul>
            </div>
        </div>
        </nav>
        <div class="container-fluid">
            @if(session('errmsg') != null)
            <div class="d-flex flex-row mt-3">
                <div class="alert alert-danger alert-dismissible fade show flex-fill" role="alert">
                    {{session('errmsg')}}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            </div>
            @endif
            @if(session('msg') != null)
            <div class="d-flex flex-row mt-3">
                <div class="alert alert-success alert-dismissible fade show flex-fill" role="alert">
                    {{session('msg')}}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            </div>
            @endif
            <div class="d-flex flex-row mt-3">
                @if($id == null)
                <h3>Tambah Karyawan Baru</h3>
                @else
                <h3>Edit Karyawan</h3>
                @endif
            </div>
            <div class="row mt-3">
                <div class="col">
                    <div class="card shadow">
                        <div class="card-body">
                            <form method="post" action="{{ route('employee.post') }}">
                                @csrf
                                @if($id != null)
                                <input type="hidden" name="id" value="{{$id}}"/>
                                @endif
                                <div class="row">
                                    <div class="col">
                                        <label for="nama" class="form-label">Nama Karyawan</label>
                                        @if($id == null)
                                        <input type="text" id="nama" class="form-control" name="nama" maxlength="50" required>
                                        @else
                                        <input type="text" id="nama" class="form-control" name="nama" maxlength="50" value="{{$employee->nama}}" required>
                                        @endif
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col">
                                        <label for="nama" class="form-label">Alamat</label>
                                        @if($id == null)
                                        <textarea rows="3" id="alamat" class="form-control" name="alamat"></textarea>
                                        @else
                                        <textarea rows="3" id="alamat" class="form-control" name="alamat">{{$employee->alamat}}</textarea>
                                        @endif
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col">
                                        <label for="nama" class="form-label">Email</label>
                                        @if($id == null)
                                        <input type="email" id="email" class="form-control" name="email" maxlength="100" required>
                                        @else
                                        <input type="email" id="email" class="form-control" name="email" maxlength="100" value="{{$employee->email}}" required>
                                        @endif
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col">
                                        <label for="nama" class="form-label">Password</label>
                                        @if($id == null)
                                        <input type="text" id="password" class="form-control" name="password" minlength="8" required>
                                        @else
                                        <input type="text" id="password" class="form-control" name="password" minlength="8">
                                        @endif
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col">
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer class="p-2" style="width: 100%; margin-top: 200px; background-color: #D3D3D3;">
            <div class="container my-auto">
            <div class="copyright text-center text-black my-auto">
                Copyright © Northen Stock 2023
            </div>
            </div>
        </footer>
        
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
    </body>
</html>