<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Northen Stock</title>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.js" integrity="sha512-nO7wgHUoWPYGCNriyGzcFwPSF+bPDOR+NvtOYy2wMcWkrnCNPKBcFEkU80XIN14UVja0Gdnff9EmydyLlOL7mQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <link href="https://nightly.datatables.net/css/dataTables.bootstrap5.min.css" rel="stylesheet"/>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    </head>
    <body>
        <nav class="navbar navbar-expand-lg" style="background-color: #D3D3D3;">
            <div class="container-fluid">
                <a class="navbar-brand" href="#">Northen Stock</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                    <a class="nav-link" href="{{ route('home') }}">Beranda</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="{{ route('product') }}">Daftar Stock</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="{{ route('opname') }}">Stock Opname</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="{{ route('history') }}">Riwayat</a>
                    </li>
                    @if(session('user')->role == 'Admin')
                    <li class="nav-item">
                    <a class="nav-link" href="{{ route('employee') }}">Karyawan</a>
                    </li>
                    @endif
                    <li class="nav-item">
                    <a class="nav-link" href="{{ route('message') }}">Pesan</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="{{ route('logout') }}">Logout</a>
                    </li>
                </ul>
                </div>
            </div>
        </nav>
        <div class="container-fluid">
            @if(session('errmsg') != null)
            <div class="d-flex flex-row mt-3">
                <div class="alert alert-danger alert-dismissible fade show flex-fill" role="alert">
                    {{session('errmsg')}}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            </div>
            @endif
            @if(session('msg') != null)
            <div class="d-flex flex-row mt-3">
                <div class="alert alert-success alert-dismissible fade show flex-fill" role="alert">
                    {{session('msg')}}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            </div>
            @endif
            <div class="d-flex flex-row mt-3">
                <h3>Daftar Stok</h3>
                @if(session('user')->role == 'Admin')
                <div class="d-flex flex-column ms-auto align-items-start">
                <a href="{{ route('product.edit') }}" class="btn btn-success">Tambah Produk</a>
                </div>
                <div class="d-flex flex-column ms-2 align-items-start">
                <a href="{{ route('product.export', ['tanggal' => $tanggal, 'kategori' => $kategori]) }}" class="btn btn-primary">Ekspor Data</a>
                </div>
                @else
                <div class="d-flex flex-column ms-auto align-items-start">
                <a href="{{ route('product.export', ['tanggal' => $tanggal, 'kategori' => $kategori]) }}" class="btn btn-primary">Ekspor Data</a>
                </div>
                @endif
            </div>
            <div class="row mt-3">
                <div class="col">
                    <div class="card shadow">
                        <div class="card-body">
                            <form method="get">
                                <div class="d-flex flex-row">
                                    <div class="d-flex flex-column flex-fill">
                                        <label for="kategori" class="form-label">Kategori</label>
                                        <select id="kategori" class="form-select" name="kategori">
                                            @if($kategori == "" || $kategori == null)
                                            <option value="" selected disabled></option>
                                            @else
                                            <option value="" disabled></option>
                                            @endif
                                            @foreach($kategoriList as $k)
                                            @if($kategori == $k->kategori)
                                            <option value="{{$k->kategori}}" selected>{{$k->kategori}}</option>
                                            @else
                                            <option value="{{$k->kategori}}">{{$k->kategori}}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="ms-2 d-flex flex-column flex-fill">
                                        <label for="tanggal" class="form-label">Tanggal</label>
                                        @if($tanggal == "" || $tanggal == null)
                                        <input type="date" id="tanggal" class="form-control" name="tanggal"/>
                                        @else
                                        <input type="date" id="tanggal" class="form-control" name="tanggal" value="{{$tanggal}}"/>
                                        @endif
                                    </div>
                                    <div class="ms-2 d-flex flex-col flex-lg-fill align-self-end">
                                        <div class="d-flex flex-row ms-auto">
                                            <button type="submit" class="btn btn-warning">Filter</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive mt-3">
                                    <table id="table" class="table table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Kode</th>
                                                <th>Nama Produk</th>
                                                <th>Kategori</th>
                                                <th>Stok Awal / Opname</th>
                                                <th>Produk Masuk</th>
                                                <th>Produk Keluar</th>
                                                <th>Stok Akhir</th>
                                                <th>Masa Kadaluarsa</th>
                                                @if(session('user')->role == 'Admin')
                                                <th>Menu</th>
                                                @endif
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($produk as $p)
                                            <tr>
                                                <td>{{$p->id}}</td>
                                                <td>{{$p->nama}}</td>
                                                <td>{{$p->kategori}}</td>
                                                <td>{{$p->qty_awal}}</td>
                                                <td>
                                                @for($i=0;$i<$p->produk_inven_masuk->count();$i++)
                                                {{$p->produk_inven_masuk[$i]->jumlah}} pada {{$p->produk_inven_masuk[$i]->created_at}}
                                                @if($i < $p->produk_inven_masuk->count()-1)
                                                <br/>
                                                @endif
                                                @endfor
                                                </td>
                                                <td>
                                                @for($i=0;$i<$p->produk_inven_keluar->count();$i++)
                                                {{$p->produk_inven_keluar[$i]->jumlah}} pada {{$p->produk_inven_keluar[$i]->created_at}}
                                                @if($i < $p->produk_inven_keluar->count()-1)
                                                <br/>
                                                @endif
                                                @endfor
                                                </td>
                                                <td>{{$p->qty_akhir}}</td>
                                                <td>
                                                @for($i=0;$i<$p->kadaluarsa->count();$i++)
                                                {{$p->kadaluarsa[$i]->jumlah}} kadaluarsa pada {{substr($p->kadaluarsa[$i]->kadaluarsa, 0, 10)}} (sisa {{$p->kadaluarsa[$i]->sisa_hari}} hari)
                                                @if($i < $p->kadaluarsa->count()-1)
                                                <br/>
                                                @endif
                                                @endfor
                                                </td>
                                                @if(session('user')->role == 'Admin')
                                                <td>
                                                    <a href="{{ route('product.edit', ['id' => $p->id]) }}" class="btn btn-outline-warning mt-1">Edit</a>
                                                    <a href="{{ route('product.delete', ['id' => $p->id]) }}" class="btn btn-outline-danger mt-1">Hapus</a>
                                                </td>
                                                @endif
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <footer class="p-2" style="width: 100%; margin-top: 220px; background-color: #D3D3D3;">
            <div class="container my-auto">
            <div class="copyright text-center text-black my-auto">
                Copyright © Northen Stock 2023
            </div>
            </div>
        </footer>

        <script>
            $(function() {
                let role = "{{session('user')->role}}";
                if(role == 'Admin')
                    $('#table').DataTable(
                        {
                            "order": [[ 0, "asc" ]],
                            "columnDefs": [
                                { "searchable": false, "targets": [8] },
                                { "sortable": false, "targets": [4,5,7,8] }
                            ]
                        }
                    );
                else
                    $('#table').DataTable(
                        {
                            "order": [[ 0, "asc" ]],
                            "columnDefs": [
                                { "sortable": false, "targets": [4,5,7] }
                            ]
                        }
                    );
            });
        </script>

        <script src="https://cdn.datatables.net/1.12.0/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.12.0/js/dataTables.bootstrap5.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
    </body>
</html>