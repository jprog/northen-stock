<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Northen Stock</title>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.js" integrity="sha512-nO7wgHUoWPYGCNriyGzcFwPSF+bPDOR+NvtOYy2wMcWkrnCNPKBcFEkU80XIN14UVja0Gdnff9EmydyLlOL7mQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <link href="https://nightly.datatables.net/css/dataTables.bootstrap5.min.css" rel="stylesheet"/>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    </head>
    <body>
        <nav class="navbar navbar-expand-lg" style="background-color: #D3D3D3;">
            <div class="container-fluid">
                <a class="navbar-brand" href="#">Northen Stock</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                    <a class="nav-link" href="{{ route('home') }}">Beranda</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="{{ route('product') }}">Daftar Stock</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="{{ route('opname') }}">Stock Opname</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="{{ route('history') }}">Riwayat</a>
                    </li>
                    @if(session('user')->role == 'Admin')
                    <li class="nav-item">
                    <a class="nav-link" href="{{ route('employee') }}">Karyawan</a>
                    </li>
                    @endif
                    <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="{{ route('message') }}">Pesan</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="{{ route('logout') }}">Logout</a>
                    </li>
                </ul>
                </div>
            </div>
        </nav>
        <div class="container-fluid">
            @if(session('errmsg') != null)
            <div class="d-flex flex-row mt-3">
                <div class="alert alert-danger alert-dismissible fade show flex-fill" role="alert">
                    {{session('errmsg')}}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            </div>
            @endif
            @if(session('msg') != null)
            <div class="d-flex flex-row mt-3">
                <div class="alert alert-success alert-dismissible fade show flex-fill" role="alert">
                    {{session('msg')}}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            </div>
            @endif
            <div class="d-flex flex-row mt-3">
                <h3>Pesan</h3>
            </div>
            @foreach($log as $l)
            <div class="row mt-3">
                <div class="col">
                    <div class="card shadow">
                        <div class="card-body">
                            {{$l->content}}
                            <div class="f-flex flex-row text-end mt-2">
                                @if($l->log_read->count() > 0)
                                <small class="text-secondary me-2">Sudah dibaca</small>
                                @endif
                                <a href="{{route('message.delete', ['id' => $l->id])}}" class="btn btn-danger">Hapus</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        
        <footer class="p-2" style="width: 100%; margin-top: 220px; background-color: #D3D3D3;">
            <div class="container my-auto">
            <div class="copyright text-center text-black my-auto">
                Copyright © Northen Stock 2023
            </div>
            </div>
        </footer>

        <script src="https://cdn.datatables.net/1.12.0/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.12.0/js/dataTables.bootstrap5.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
    </body>
</html>