<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Northen Stock</title>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.js" integrity="sha512-nO7wgHUoWPYGCNriyGzcFwPSF+bPDOR+NvtOYy2wMcWkrnCNPKBcFEkU80XIN14UVja0Gdnff9EmydyLlOL7mQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <link href="https://nightly.datatables.net/css/dataTables.bootstrap5.min.css" rel="stylesheet"/>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    </head>
    <body>
        <nav class="navbar navbar-expand-lg" style="background-color: #D3D3D3;">
            <div class="container-fluid">
                <a class="navbar-brand" href="#">Northen Stock</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                    <a class="nav-link" href="{{ route('home') }}">Beranda</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="{{ route('product') }}">Daftar Stock</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="{{ route('opname') }}">Stock Opname</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="{{ route('history') }}">Riwayat</a>
                    </li>
                    @if(session('user')->role == 'Admin')
                    <li class="nav-item">
                    <a class="nav-link" href="{{ route('employee') }}">Karyawan</a>
                    </li>
                    @endif
                    <li class="nav-item">
                    <a class="nav-link" href="{{ route('message') }}">Pesan</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="{{ route('logout') }}">Logout</a>
                    </li>
                </ul>
                </div>
            </div>
        </nav>
        <div class="container-fluid">
            @if(session('errmsg') != null || $errmsg != null)
            <div class="d-flex flex-row mt-3">
                <div class="alert alert-danger alert-dismissible fade show flex-fill" role="alert">
                    {{session('errmsg') ?: $errmsg}}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            </div>
            @endif
            @if(session('msg') != null || $msg != null)
            <div class="d-flex flex-row mt-3">
                <div class="alert alert-success alert-dismissible fade show flex-fill" role="alert">
                    {{session('msg') ?: $msg}}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            </div>
            @endif
            <div class="d-flex flex-row mt-3">
                <h3>Stock Opname</h3>
                @if($opname != null)
                @if($opname->opname_detail->count() > 0)
                <div class="d-flex flex-column ms-auto align-items-start">
                <a href="{{ route('opname.export', ['tanggal' => $tanggal, 'kategori' => $kategori]) }}" class="btn btn-primary">Cetak Stock Opname</a>
                </div>
                @endif
                @endif
            </div>
            <div class="row mt-3">
                <div class="col">
                    <div class="card shadow">
                        <div class="card-body">
                                <form method="get">
                                <div class="d-flex flex-row">
                                    <div class="d-flex flex-column flex-fill">
                                        <label for="kategori" class="form-label">Kategori</label>
                                        <select id="kategori" class="form-select" name="kategori">
                                            @if($kategori == "" || $kategori == null)
                                            <option value="" selected disabled></option>
                                            @else
                                            <option value="" disabled></option>
                                            @endif
                                            @foreach($kategoriList as $k)
                                            @if($kategori == $k->kategori)
                                            <option value="{{$k->kategori}}" selected>{{$k->kategori}}</option>
                                            @else
                                            <option value="{{$k->kategori}}">{{$k->kategori}}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="ms-2 d-flex flex-column flex-fill">
                                        <label for="tanggal" class="form-label">Tanggal</label>
                                        @if($tanggal == "" || $tanggal == null)
                                        <input type="date" id="tanggal" class="form-control" name="tanggal"/>
                                        @else
                                        <input type="date" id="tanggal" class="form-control" name="tanggal" value="{{$tanggal}}"/>
                                        @endif
                                    </div>
                                    <div class="ms-2 d-flex flex-col flex-lg-fill align-self-end">
                                        <div class="d-flex flex-row ms-auto">
                                            <button type="submit" class="btn btn-warning">Filter</button>
                                        </div>
                                    </div>
                                </div>
                                </form>
                                <form method="post" action="{{route('opname.post')}}">
                                @csrf
                                @if($tanggal != null)
                                <input type="hidden" name="tanggal" value="{{$tanggal}}" />
                                @else
                                <input type="hidden" name="tanggal" value="{{\Carbon\Carbon::now()->format('Y-m-d')}}" />
                                @endif
                                <div class="table-responsive mt-3">
                                    <table id="table" class="table table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Kode</th>
                                                <th>Nama Produk</th>
                                                <th>Kategori</th>
                                                <th>Stok Awal / Opname</th>
                                                <th>Produk Masuk</th>
                                                <th>Produk Keluar</th>
                                                <th>Stok Akhir</th>
                                                <th>Stok Aktual</th>
                                                <th>Selisih</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $continue = true;
                                            @endphp
                                            @if($opname != null)
                                            @if($opname->opname_detail->count() > 0)
                                            @php
                                            $continue = false;
                                            @endphp

                                            @foreach($produk as $p)
                                            <tr>
                                                @php
                                                $od = $p->opname_detail->where('opname_id', $opname->id)->last();
                                                @endphp
                                                <td>{{$p->id}}</td>
                                                <td>{{$p->nama}}</td>
                                                <td>{{$p->kategori}}</td>
                                                @if($od != null)
                                                <td>{{$od->qty_awal}}</td>
                                                @else
                                                <td>{{$p->qty_awal}}</td>
                                                @endif
                                                <td>
                                                @if($od != null && $od->opname_detail_produk != null)
                                                @for($i=0;$i<$od->opname_detail_produk->count();$i++)
                                                @if($od->opname_detail_produk[$i]->jenis == 'Masuk')
                                                {{$od->opname_detail_produk[$i]->content}}
                                                <br/>
                                                @endif
                                                @endfor
                                                @else
                                                @for($i=0;$i<$p->produk_inven_masuk->count();$i++)
                                                {{$p->produk_inven_masuk[$i]->jumlah}} pada {{$p->produk_inven_masuk[$i]->created_at}}
                                                @if($i < $p->produk_inven_masuk->count()-1)
                                                <br/>
                                                @endif
                                                @endfor
                                                </td>
                                                @endif
                                                <td>
                                                @if($od != null && $od->opname_detail_produk != null)
                                                @for($i=0;$i<$od->opname_detail_produk->count();$i++)
                                                @if($od->opname_detail_produk[$i]->jenis == 'Keluar')
                                                {{$od->opname_detail_produk[$i]->content}}
                                                <br/>
                                                @endif
                                                @endfor
                                                @else
                                                @for($i=0;$i<$p->produk_inven_keluar->count();$i++)
                                                {{$p->produk_inven_keluar[$i]->jumlah}} pada {{$p->produk_inven_keluar[$i]->created_at}}
                                                @if($i < $p->produk_inven_keluar->count()-1)
                                                <br/>
                                                @endif
                                                @endfor
                                                @endif
                                                </td>
                                                @if($od != null)
                                                <td>{{$od->qty_system}}</td>
                                                @else
                                                <td>{{$p->qty_akhir}}</td>
                                                @endif
                                                @if($od != null)
                                                <td>{{$od->qty_actual}}</td>
                                                @else
                                                <td></td>
                                                @endif
                                                @if($od != null)
                                                <td>{{abs($od->qty_actual-$od->qty_system)}}</td>
                                                @else
                                                <td></td>
                                                @endif
                                            </tr>
                                            @endforeach
                                            
                                            @endif
                                            @endif

                                            @if($opname == null || $continue)
                                            @php
                                                $index = 0;
                                                $odp_contents = [];
                                                $odp_jenises = [];
                                            @endphp
                                            @foreach($produk as $p)
                                            @php
                                                array_push($odp_contents, "");
                                                array_push($odp_jenises, "");
                                            @endphp
                                            <tr>
                                                <td><input type="hidden" name="produk_id[]" value="{{$p->id}}"/>{{$p->id}}</td>
                                                <td>{{$p->nama}}</td>
                                                <td>{{$p->kategori}}</td>
                                                <td><input type="hidden" name="qty_awal[]" value="{{$p->qty_awal}}"/>{{$p->qty_awal}}</td>
                                                <td>
                                                @for($i=0;$i<$p->produk_inven_masuk->count();$i++)
                                                {{$p->produk_inven_masuk[$i]->jumlah}} pada {{$p->produk_inven_masuk[$i]->created_at}}
                                                @php
                                                    $odp_contents[$index] .= $p->produk_inven_masuk[$i]->jumlah.' pada '.$p->produk_inven_masuk[$i]->created_at;
                                                    $odp_jenises[$index] .= 'Masuk';
                                                @endphp
                                                @if($i < $p->produk_inven_masuk->count()-1)
                                                @php
                                                    $odp_contents[$index] .= ';';
                                                    $odp_jenises[$index] .= ';';
                                                @endphp
                                                <br/>
                                                @else
                                                @php
                                                    $odp_contents[$index] .= ';';
                                                    $odp_jenises[$index] .= ';';
                                                @endphp
                                                @endif
                                                @endfor
                                                </td>
                                                <td>
                                                @for($i=0;$i<$p->produk_inven_keluar->count();$i++)
                                                {{$p->produk_inven_keluar[$i]->jumlah}} pada {{$p->produk_inven_keluar[$i]->created_at}}
                                                @php
                                                    $odp_contents[$index] .= $p->produk_inven_keluar[$i]->jumlah.' pada '.$p->produk_inven_keluar[$i]->created_at;
                                                    $odp_jenises[$index] .= 'Keluar';
                                                @endphp
                                                @if($i < $p->produk_inven_keluar->count()-1)
                                                @php
                                                    $odp_contents[$index] .= ';';
                                                    $odp_jenises[$index] .= ';';
                                                @endphp
                                                <br/>
                                                @endif
                                                @endfor
                                                <input type="hidden" name="odp_content[]" value="{{$odp_contents[$index]}}"/>
                                                <input type="hidden" name="odp_jenis[]" value="{{$odp_jenises[$index]}}"/>
                                                </td>
                                                <td><input type="hidden" name="qty_akhir[]" value="{{$p->qty_akhir}}"/>{{$p->qty_akhir}}</td>
                                                <td><input type="number" min="0" name="qty_actual[]" class="form-control"/></td>
                                                <td><input type="number" readonly id="sisa" class="form-control"/></td>
                                            </tr>
                                            @php
                                            $index++;
                                            @endphp
                                            @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                                @if($continue && count($produk) > 0)
                                <div class="d-flex flex-col flex-lg-fill align-self-end mt-3">
                                    <div class="d-flex flex-row mx-auto">
                                        <button type="submit" class="btn btn-success">Simpan</button>
                                    </div>
                                </div>
                                @endif
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <footer class="p-2" style="width: 100%; margin-top: 220px; background-color: #D3D3D3;">
            <div class="container my-auto">
            <div class="copyright text-center text-black my-auto">
                Copyright © Northen Stock 2023
            </div>
            </div>
        </footer>

        <script>
            $(function() {
                $('#table').DataTable(
                    {
                        paging: false,
                        "order": [[ 0, "asc" ]],
                        "columnDefs": [
                            { "sortable": false, "targets": [4,5,7,8] }
                        ]
                    }
                );
            });
        </script>

        <script src="https://cdn.datatables.net/1.12.0/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.12.0/js/dataTables.bootstrap5.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
    </body>
</html>