<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Northen Stock</title>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.min.js" integrity="sha512-STof4xm1wgkfm7heWqFJVn58Hm3EtS31XFaagaa8VMReCXAkQnJZ+jEy8PCC/iT18dFy95WcExNHFTqLyp72eQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    </head>
    <body>
        <nav class="navbar navbar-expand-lg" style="background-color: #D3D3D3;">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Northen Stock</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="{{ route('home') }}">Beranda</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="{{ route('product') }}">Daftar Stock</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="{{ route('opname') }}">Stock Opname</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="{{ route('history') }}">Riwayat</a>
                </li>
                @if(session('user')->role == 'Admin')
                <li class="nav-item">
                <a class="nav-link" href="{{ route('employee') }}">Karyawan</a>
                </li>
                @endif
                <li class="nav-item">
                <a class="nav-link" href="{{ route('message') }}">Pesan</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="{{ route('logout') }}">Logout</a>
                </li>
                </li>
            </ul>
            </div>
        </div>
        </nav>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 col-12 mx-md-2 mx-1">
                    @if(session('errmsg') != null)
                    <div class="alert alert-danger alert-dismissible fade show mt-3" role="alert">
                        {{session('errmsg')}}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    @endif
                    @if(session('msg') != null)
                    <div class="alert alert-success alert-dismissible fade show mt-3" role="alert">
                        {{session('msg')}}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    @endif
                    <h3 class="mt-3">Penerimaan Produk</h3>
                    <div class="card shadow mt-3">
                        <div class="card-body">
                            <form action="{{route('home.masuk')}}" method="post" id="formMasuk">
                            @csrf
                            <div class="row" id="masuk1">
                                <div class="col-4">
                                    <label for="produk" class="form-label">Pilih Produk</label>
                                    <select id="produk" class="form-select" name="produk_id[]" required>
                                        <option value="" disabled selected></option>
                                        @foreach($produk as $p)
                                        <option value="{{$p->id}}">{{$p->nama.' (ID: '.$p->id.')'}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-4">
                                    <label for="jumlah" class="form-label">Jumlah Produk</label>
                                    <input type="number" min="1" id="jumlah" class="form-control" name="jumlah[]" required/>
                                </div>
                                <div class="col-4">
                                    <label for="kadaluarsa" class="form-label">Kadaluarsa</label>
                                    <input type="date" id="kadaluarsa" class="form-control" name="kadaluarsa[]" required/>
                                </div>
                            </div>
                            <div class="row justify-content-center mt-2">
                                <div class="col-12 text-end">
                                <button type="button" class="btn btn-danger" onclick="kurangMasuk()">Kurang</button>
                                <button type="button" class="btn btn-success" onclick="tambahMasuk()">Tambah</button>
                                </div>
                            </div>
                            <div class="row justify-content-center mt-2">
                                <div class="col-12 text-center">
                                <button type="submit" class="btn btn-primary">Masuk Stok</button>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                    <h3 class="mt-3">Pengeluaran Produk</h3>
                    <div class="card shadow mt-3">
                        <div class="card-body">
                            <form action="{{route('home.keluar')}}" method="post" id="formKeluar">
                            @csrf
                            <div class="row" id="keluar1">
                                <div class="col-4">
                                    <label for="produk_kel1" class="form-label">Pilih Produk</label>
                                    <select id="produk_kel1" class="form-select" name="produk_id[]" required>
                                        <option value="" disabled selected></option>
                                        @foreach($produk as $p)
                                        <option value="{{$p->id}}">{{$p->nama.' (ID: '.$p->id.')'}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-4">
                                    <label for="jumlah_kel1" class="form-label">Jumlah Produk</label>
                                    <input type="number" min="1" id="jumlah_kel1" class="form-control" name="jumlah[]" required/>
                                </div>
                                <div class="col-4">
                                    <label for="sisa1" class="form-label">Sisa Produk</label>
                                    <input type="number" min="0" id="sisa1" class="form-control" readonly/>
                                </div>
                            </div>
                            <div class="row justify-content-center mt-2">
                                <div class="col-12 text-end">
                                <button type="button" class="btn btn-danger" onclick="kurangKeluar()">Kurang</button>
                                <button type="button" class="btn btn-success" onclick="tambahKeluar()">Tambah</button>
                                </div>
                            </div>
                            <div class="row justify-content-center mt-2">
                                <div class="col-12 text-center">
                                <button type="submit" class="btn btn-primary">Keluar Stok</button>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-12 mx-md-2 mx-1">
                    <h3 class="mt-3">Masa Kadaluarsa</h3>
                    @foreach($expInven as $exp)
                    <div class="card shadow mt-3">
                        <div class="card body p-3">
                            <h3>{{$exp->produk->nama.' (ID: '.$exp->produk->id.')'}}</h3>
                            <h5>{{'Jumlah Produk Masuk: '.$exp->jumlah}}</h5>
                            <h5 class="text-danger">{{'Sisa: '.$exp->sisa_hari.' hari'}}</h5>
                            <h6>{{'Kadaluarsa: '.substr($exp->kadaluarsa, 0, 10)}}</h6>
                            <form action="{{route('home.masuk.update')}}" method="post">
                                @csrf
                                <input type="hidden" name="id" value="{{$exp->id}}"/>
                                <button type="submit" class="btn btn-warning">Sudah Habis</button>
                            </form>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>

        <script>
            var itemMas = 1;
            var itemKel = 1;
            let daftarProduk = @json($produk);
            function hitungSisa(id) {
                let jumlah = $('#jumlah_kel'+id).val();
                let produk = $('#produk_kel'+id).val();
                if(jumlah != null && produk != null) {
                    if(jumlah.length > 0 && produk.length > 0) {
                        var sisa = 0;
                        for(var i=0; i<daftarProduk.length; i++) {
                            if(daftarProduk[i].id == produk) {
                                sisa = daftarProduk[i].qty_akhir;
                                $("#jumlah_kel"+id).attr({
                                    "max" : daftarProduk[i].qty_akhir
                                });
                                break;
                            }
                        }
                        $('#sisa'+id).val(sisa-jumlah);
                    }
                }
            }
            $('#jumlah_kel1').on('change', function() {
                hitungSisa(1);
            });
            $('#produk_kel1').on('change', function() {
                hitungSisa(1);
            });
            function tambahKeluar() {
                itemKel++;
                $(`
                    <div class="row mt-2" id="keluar`+itemKel+`">
                        <div class="col-4">
                            <label for="produk_kel`+itemKel+`" class="form-label">Pilih Produk</label>
                            <select id="produk_kel`+itemKel+`" class="form-select" name="produk_id[]" required>
                                <option value="" disabled selected></option>
                                @foreach($produk as $p)
                                <option value="{{$p->id}}">{{$p->nama.' (ID: '.$p->id.')'}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-4">
                            <label for="jumlah_kel`+itemKel+`" class="form-label">Jumlah Produk</label>
                            <input type="number" min="1" id="jumlah_kel`+itemKel+`" class="form-control" name="jumlah[]" required/>
                        </div>
                        <div class="col-4">
                            <label for="sisa`+itemKel+`" class="form-label">Sisa Produk</label>
                            <input type="number" min="0" id="sisa`+itemKel+`" class="form-control" readonly/>
                        </div>
                    </div>
                `).insertAfter("#keluar"+(itemKel-1));
                $('#jumlah_kel'+itemKel).on('change', function() {
                    hitungSisa(itemKel);
                });
                $('#produk_kel'+itemKel).on('change', function() {
                    hitungSisa(itemKel);
                });
            }
            function tambahMasuk() {
                itemMas++;
                $(`
                    <div class="row mt-2" id="masuk`+itemMas+`">
                        <div class="col-4">
                            <label for="produk" class="form-label">Pilih Produk</label>
                            <select id="produk" class="form-select" name="produk_id[]" required>
                                <option value="" disabled selected></option>
                                @foreach($produk as $p)
                                <option value="{{$p->id}}">{{$p->nama.' (ID: '.$p->id.')'}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-4">
                            <label for="jumlah" class="form-label">Jumlah Produk</label>
                            <input type="number" min="1" id="jumlah" class="form-control" name="jumlah[]" required/>
                        </div>
                        <div class="col-4">
                            <label for="kadaluarsa" class="form-label">Kadaluarsa</label>
                            <input type="date" id="kadaluarsa" class="form-control" name="kadaluarsa[]" required/>
                        </div>
                    </div>
                `).insertAfter("#masuk"+(itemMas-1));
            }
            function kurangKeluar() {
                if(itemKel == 1) {
                    alert("Tidak dapat mengurangi masukan produk keluar");
                    return;
                }
                $("#keluar"+itemKel).remove();
                itemKel--;
            }
            function kurangMasuk() {
                if(itemMas == 1) {
                    alert("Tidak dapat mengurangi masukan produk masuk");
                    return;
                }
                $("#masuk"+itemMas).remove();
                itemMas--;
            }
        </script>

        <footer class="p-2" style="width: 100%; margin-top: 50px; background-color: #D3D3D3;">
            <div class="container my-auto">
            <div class="copyright text-center text-black my-auto">
                Copyright © Northen Stock 2023
            </div>
            </div>
        </footer>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
    </body>
</html>